#  Data Warehousing


## What is Data Warehousing?
 To understand the Data warehouse, we need to understand the term Warehouse:<br>
   A warehouse is a place, where things get collected and stored for some purpose

Data warehouse is also the same, it is used to store the data for analysis, and enterprises generally use it to store the historical Data. so, that enterprises can decides on behalf of this data 

![Data Warehouse](https://streamsets.b-cdn.net/wp-content/uploads/data-warehouse-architecture-768x409.png)

## Advantages of Data Warehouse

- **Ease of Data Access**: Data warehouses make it easy for users to access and analyze data efficiently.

- **Handling Large Data Volumes**: They can handle and process large amounts of data, enabling comprehensive analysis and reporting.

- **Supports Informed Decision Making**: By providing access to historical and aggregated data, data warehouses empower businesses to make informed decisions based on past performance and trends, ultimately enhancing productivity.

- **Enhanced Connectivity and Cost Reduction**: Data warehouses facilitate better connectivity among departments, leading to streamlined operations and improved supply chain management, which can reduce overall operational costs.


## Disadvantages of Data Warehouse
- Manage data warehouse is so costly, because of the large amount of data 

- Building and maintaining a data warehouse is complex. requiring specialized skills and expertise in data modeling

- Data Warehouses depend heavily on input data, If source data is incomplete, inconsistent, or inaccurate, it can affect the quality of analytics and decision-making based on the warehouse data.

- Centralizing large volumes of data in a data warehouse can pose security risks if proper security measures are not in place. Unauthorized access or data breaches can compromise sensitive business information.



## cloud-based data warehouse Vs. Traditional  

- Cloud data warehouses can easily scale up or down based on demand. They can handle large volumes of data and support increased workloads without requiring significant hardware investments or infrastructure changes. but it is not possible in traditional data warehouses because scale-up needs a large amount of investments

- Cloud data warehouses offer more flexibility in terms of data storage and computing resources. They allow organizations to quickly adapt to changing business needs, experiment with new analytics tools, and easily integrate with other cloud services. but traditional data warehouses do not this type of flexibility

- Cloud data warehouses operate on a pay-as-you-go pricing model, where organizations pay only for the resources they use. This eliminates the need for upfront hardware costs and reduces ongoing maintenance expenses in traditional data warehouses when we need more storage we have to invest in the system




## References

- [google cloud](https://cloud.google.com/learn/what-is-a-data-warehouse)

- [oracle Website](https://www.oracle.com/in/database/what-is-a-data-warehouse/)

- [JavaTpoint](https://www.javatpoint.com/data-warehouse)
