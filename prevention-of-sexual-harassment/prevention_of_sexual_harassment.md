# Prevention of Sexual Harassment
  

## What kinds of behaviour cause sexual harassment?
   Sexual harassment can vary based on individual experiences and perceptions. Common behaviors like inappropriate hugging, making sexual comments, or displaying inappropriate material are indeed widely recognized as forms of sexual harassment. It's important to acknowledge that people may interpret and experience harassment differently, but certain behaviors are generally understood as inappropriate and potentially harmful.


## What would you do in case you face or witness any incident or repeated incidents of such behaviour?
 - ### Report to Supervisor:
    - Inform your supervisor or manager about the incident or repeated incidents.
- ### Escalate to HR or Higher Authorities:
    - If the supervisor does not take the complaint seriously or fails to address the issue, escalate the matter to the HR department or higher-level authorities within your organization.
- ### Seek External Help if Needed:
    - If internal avenues are exhausted without resolution or if the situation warrants immediate action, consider reaching out to local law enforcement or relevant external authorities for further assistance.



