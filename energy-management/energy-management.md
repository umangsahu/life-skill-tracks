# Energy-management

### 1. What are the activities you do that make you relax - Calm quadrant?
- Going for a leisurely walk in nature.
- Listening to music.
- spend time with family and friends.


### 2. When do you find getting into the Stress quadrant?
- Facing financial difficulties or unexpected expenses.
- Dealing with a heavy workload.
- Coping with health issues or illness.
- Dealing with unexpected problems or conflicts.
- Approaching deadlines.


### 3.  How do you understand if you are in the Excitement quadrant?
- I feel energized and eager about upcoming opportunities.
- I view challenges as adventures rather than obstacles.
- I am inspired and motivated to take on new tasks or projects.


### 4. Paraphrase the Sleep is your Superpower video in your own words in brief?
- Sufficient sleep can alleviate anxiety.
- Adequate sleep plays a role in decreasing the risk of heart problems.
- Quality sleep is associated with a longer lifespan.
- Sleep has been shown to enhance creativity.

### 5. What are some ideas that you can implement to sleep better?
- Avoid caffeine and heavy meals close to bedtime.
- Keep your bedroom cool, quiet, and dark to promote better sleep.
- Consider using white noise machines or earplugs to block out disruptive noises.
- Limit daytime naps to avoid interfering with your nighttime sleep schedule.
- Seek professional help if you consistently struggle with sleep issues or insom

### 6. Paraphrase the video - Brain Changing Benefits of Exercise?
- Physical activity enhances brain function.
- Exercise provides beneficial elements to the brain.
- Engaging in exercise promotes brain growth.
- Regular exercise can improve cognitive abilities.
- Physical activity maintains the strength of our brain.

### 7. What are some steps you can take to exercise more?
- Start small and gradually increase activity.
- Choose activities you enjoy.
- Schedule exercise like any other appointment.
- Mix up your routine to stay engaged.
- Find a workout buddy for accountability.
