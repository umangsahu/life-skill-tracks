# Listening and Active Communication

## What are the steps/strategies to do Active Listening?
- Stay focused and avoid getting distracted by your own thoughts.
- Use verbal cues like "Sounds interesting" or "I'm listening" to show engagement.
- Employ open-ended prompts such as "Tell me more" to encourage further discussion.
- Demonstrate listening through positive body language like nodding and maintaining eye contact.
- Take notes during important conversations to capture key points and demonstrate attentiveness.

## According to Fisher's model, what are the key points of Reflective Listening?

- **Active Engagement**: Show full engagement with the speaker by listening attentively without interrupting.

- **Understanding and Empathy**: Strive to understand the speaker's perspective and empathize with their feelings and emotions.

- **Paraphrasing**: Summarize or restate the speaker's message in your own words to demonstrate comprehension and ensure accuracy.

- **Validation**: Acknowledge and validate the speaker's feelings and thoughts without passing judgment or offering advice.

## What are the obstacles in your listening process?

- **Too Many Noises**: It's hard to hear when there's a lot of background noise.

- **Thinking About Other Things**: When we're thinking about something else, we might not hear what's being said.

- **Feeling Upset**: Being upset makes it difficult to focus on what someone else is saying.

- **Not Paying Attention**: Sometimes, we're not really listening because we're not paying attention.

## What can you do to improve your listening?

- **Quiet Place**: Find a quiet spot where there aren't many noises to distract you.

- **Look and Listen**: Pay attention to the person speaking and look at them when they talk.

- **Ask Questions**: If you don't understand something then ask questions to learn more.

- **Practice**: Try listening more when people talk, and it will get easier over time.

## When do you switch into Aggressive communication styles in your day to day life?
- **Expressing Anger**: Aggressive communication can arise when individuals express anger in a confrontational manner.

- **Dominating Conversations**: Aggressive communication may occur when someone monopolizes or controls a conversation, disregarding others' input.

- **Ignoring Feelings**: Aggressive communicators may dismiss or invalidate others' feelings, showing a lack of empathy and understanding.

## When do you switch into Passive Aggressive communication styles in your day to day life?

- while using sarcasm.
- while Gossiping about some topics.
- while Making subtle, snide remarks to irritate or provoke others.

## How can you make your communication assertive?

- Clearly express your thoughts, feelings, and needs without being aggressive or passive.

- Speak from your own perspective using "I feel" or "I think" to own your feelings and opinions.

- Pay attention to others' viewpoints and respond thoughtfully without interrupting.

- Clearly communicate your limits and expectations to maintain respect and mutual understanding.
