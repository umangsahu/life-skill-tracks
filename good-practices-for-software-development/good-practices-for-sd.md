# Good practices for Software Development

### 1.Which point(s) were new to you?

The points that were new to me were :

- We can track our time using app like "Boosted" to improve productivity.
- We can use Github gists to share code snippets. and can use sandbox environments like Codepen, Codesandbox to share the entire setup



### 2. Which area do you think you need to improve on? What are your ideas to make progress in that area?

 **Areas where I think I need to improve are as follows:**

- time management
- focus management
- communicating on Software requirement
- tracking time while working and take efficient breaks.


**Ideas to make progress in that particular area are as follows:**

- Utilize time tracking tools such as "Boosted".
-  Practice mindfulness techniques like meditation regularly while minimizing distractions in your environment.
- Make notes while discussing requirements with your team.


