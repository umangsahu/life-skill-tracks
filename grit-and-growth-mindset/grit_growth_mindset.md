# Understanding Grit, Growth Mindset

## 1.Grit
Grit is a combination of passion and perseverance in an effort to achieve long-term goals. Without passion, perseverance leads to burnout. And without perseverance, we simply give up.
Grit gives people a mental toughness that enables them to persist — and even succeed — in the face of adversity.

## 2.Introduction to Growth Mindset
The growth mindset is a belief that abilities can be developed through effort and perseverance. It fosters resilience in the face of challenges, viewing setbacks as opportunities for learning and growth. Unlike the fixed mindset, it embraces the idea of continuous improvement and exploration. Cultivating a growth mindset empowers individuals to reach their full potential and thrive in various aspects of life. It's a powerful mindset shift that encourages resilience, learning, and achievement.

## 3. What is the Internal Locus of Control?
Internal locus of control refers to the belief that one has control over their own life and the outcomes of their actions. In the context of software development, understanding and cultivating an internal locus of control can lead to greater motivation, resilience, and success. The provided video emphasizes the significance of internal locus of control in maintaining motivation and achieving goals.

## 4. What are the key points mentioned by speaker to build growth mindset?
- Believe in your ability to figure things out.
- Question your assumptions
- Create your own curriculum for growth
- Honor the struggle


## 5.What are your ideas to take action and build Growth Mindset?
- Dare to take on more complex tasks involving the unknown and persevere with them. Challenges are opportunities to learn and strengthen your skills and resilience.
- Try out new technologies, work methodologies, and design patterns. Don't be afraid to make mistakes; they're opportunities to learn.
- Remain open to constructive criticism. This will help you gain new perspectives and improve.



##  References
- Duckworth, Angela. "Grit: The power of passion and perseverance." Scribner, 2016.
- Dweck, Carol S. "Mindset: The new psychology of success." Random House, 2006.
- [Insert references for provided videos]
