# Tiny-habbits

### 1. In this video, what was the most interesting story or idea for you?

- In the video, one highlight was when BJ Fogg shared an anecdote about a woman aiming to floss more consistently. Rather than attempting to floss all her teeth in one go, she started by flossing just one tooth nightly. This small step proved effortless, encouraging her to continue. Eventually, she found herself flossing more teeth without conscious effort. This story underscored the power of starting with tiny actions to cultivate significant habit changes.



### 2. How can you use B = MAP to make making new habits easier? What are M, A and P

When you're trying to make a new habit, you want to think about why you want to do it, make it as possible to do, and set up reminders or cues to help you remember to do it. When you combine these things, it makes forming new habits a lot easier.

- M-Motivation: This is about why you want to do something. It's the reason that makes you want to start a new habit. It could be because you enjoy it, or it's important to you in some way.
- A - Ability: This is about how easy or hard it is to do something. If something is really easy, you're more likely to do it. So, to make a new habit easier, you want to make it as simple as possible.
- P - Prompt: This is like a reminder or a signal that tells you to do something. It could be a reminder on your phone, or something in your environment that reminds you to do the habit.



### 3. Why it is important to "Shine" or Celebrate after each successful completion of habit?

- Celebrating success after completing a habit is crucial for several reasons. First, it reinforces the habit loop by providing a satisfying reward, making it more likely to be repeated in the future. Second, it boosts motivation and confidence, reminding us of our capability to achieve our goals. Finally, celebrating success serves as a marker of progress, helping us track our journey towards our larger goals and fostering a sense of accomplishment.


### 4. In the 1% Better Every Day Video, what was the most interesting story or idea for you?
- The video highlights the power of small daily improvements, like doing five push-up a day, to achieve big goals over time. It shows that consistent, tiny steps can lead to significant changes.

In conclusion, the video underscores the transformative potential of consistent, incremental improvements in achieving significant long-term goals, inspiring a mindset of continuous growth and progress.


### 5. What is the book's perspective about Identity?

- The book says if you want to make good habits stick, think about the kind of person you want to be. When your habits match that image, change becomes easier and lasts longer.

---

### 6. Write about the book's perspective on how to make a habit easier to do?
- To make a habit easier, the book suggests cutting down obstacles, making it clear, starting small, linking it to existing habits, and setting up your surroundings to remind you.

### 7. Write about the book's perspective on how to make a habit harder to do?
- To make a habit tougher, the book advises adding obstacles, hiding it, making it more complex, and using tricks to commit to avoiding unwanted behaviors.



### 8.  Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

- Make the habit attractive: Choose activities I enjoy, like dancing or hiking with friends.
- Make it easy: Start with short, manageable workouts and gradually increase intensity.
- Make the cue obvious: Place my workout clothes and shoes next to my bed the night before.
- Make the response satisfying: Reward myself after workouts with something I enjoy, like a healthy snack or a relaxing bath.
By implementing these steps, I can make it easier to avoid social media and develop the habit of coding, ultimately leading to increased productivity and skill development.


### 9. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

One habit i would like to eliminate is laziness and distraction to cheap dopamine

- Hide social media apps and set usage limits.
- Reflect on negative effects and replace with fulfilling activities.
- Keep phone away during tasks.
- Swap social media time with better activities.
- Reduce dependence gradually.

